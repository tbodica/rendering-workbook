## opengl-workbook

Workbook for OpenGL in Python.

![Screenshot1](Images/screenshot_01.PNG)

If you want to try this on windows, please note that I ran into issues with the packages.

## Dependencies

- https://pypi.org/project/pygame/
- https://pypi.org/project/PyOpenGL/

## About

Scripts in python for interfacing OpenGL with the help of a library called PyOpenGL, inspired by youtube lessons from user 'sentdex'.

Methods are similar to what you encounter if you were to use it in C++, however the environment looks simpler in python.